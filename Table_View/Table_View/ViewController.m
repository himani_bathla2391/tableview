//
//  ViewController.m
//  Table_View
//
//  Created by Clicklabs 104 on 9/29/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *data;
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *currentTable;
@property (weak, nonatomic) IBOutlet UITextField *text;
@property (weak, nonatomic) IBOutlet UIButton *enter;

@end

@implementation ViewController
//NSMutableArray *data;
@synthesize currentTable;
@synthesize text;
@synthesize enter;
- (void)viewDidLoad {
    [super viewDidLoad];
    data=[[NSMutableArray alloc]init];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.jpeg"]]];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}
- (IBAction)press:(id)sender {
    [data addObject:text.text];
    [currentTable reloadData];
}

     
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return data.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellReuse"];
    cell.textLabel.text = data[indexPath.row];
    return cell;
    
}

@end
