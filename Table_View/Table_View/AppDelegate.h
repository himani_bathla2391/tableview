//
//  AppDelegate.h
//  Table_View
//
//  Created by Clicklabs 104 on 9/29/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

