//
//  ViewController.h
//  Table_View
//
//  Created by Clicklabs 104 on 9/29/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>


@end

